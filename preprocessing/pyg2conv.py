#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from argparse import ArgumentParser
from pathlib import Path

import torch
from tqdm import tqdm
from torch_geometric.data import Data

parser = ArgumentParser()

parser.add_argument(
    '--input', '-i', 
    type=Path,
    required=True,
    help='Input directory containing event file'
)

parser.add_argument(
    '--output', '-o',
    type=Path,
    help='Output directory. Overwrite original data if not specify.'
)

args = parser.parse_args()

source = args.input
target = args.output or source
target.mkdir(parents=True, exist_ok=True)

for file in tqdm(list(source.glob('*'))):
    data = torch.load(
        file, map_location="cpu"
    )
    converted = Data.from_dict(
        data.__dict__
    )
    torch.save(
        converted, target/file.name
    )

