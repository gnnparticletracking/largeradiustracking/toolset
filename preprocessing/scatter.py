from pathlib import Path

import numpy as np
import pandas as pd
import torch
from matplotlib import pyplot as plt

input_dir = Path('/pscratch/sd/i/ianwang/LRT/v3/pileup40/filter_processed/train')

count = np.array([
    [len(torch.load(path, map_location='cpu')['y']),
     len(torch.load(path, map_location='cpu')['hid'])]
    for path in input_dir.glob("*")
])
fig, ax = plt.subplots(
    1, 1, figsize=(8, 8), tight_layout=True
)
ax.scatter(count[:,1], count[:,0])
ax.set_xlabel('#nodes')
ax.set_ylabel('#edges')

fig.savefig('edge_node_dist.pdf')

