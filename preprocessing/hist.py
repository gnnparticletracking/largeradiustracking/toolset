from argparse import ArgumentParser
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from matplotlib import pyplot as plt

input_dir = Path('/pscratch/sd/i/ianwang/LRT/v3/pileup40/filter_processed/train')

edge_count = [
    len(torch.load(path, map_location='cpu')['y']) 
    for path in input_dir.glob("*")
]

fig, ax = plt.subplots(
    1, 1, figsize=(8, 8), tight_layout=True      
)
ax.hist(edge_count, 50)
ax.set_xlabel("#Edges")
ax.set_ylabel("Count")

fig.savefig('edge_count_dist.pdf')

