#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from argparse import ArgumentParser
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from tqdm import tqdm


parser = ArgumentParser()

parser.add_argument(
    '--input', '-i', 
    type=Path,
    required=True,
    help='Input directory containing event file'
)

parser.add_argument(
    '--particles', '-p',
    type=Path,
    help=(
        'Input directory contatining particle information, '
        'named as eventXXXXXXXXX-particles.csv. '
        'This is an optional argument. '
        'The script will try to assign transverse momentum (pt) to output data '
        'if you provide this information.'
    )
)

parser.add_argument(
    '--output', '-o',
    type=Path,
    required=True,
    help='Output directory'
)

args = parser.parse_args()

event_source = args.input
particle_source = args.particles

target = args.output
target.mkdir(parents=True, exist_ok=True)

normalize_factor = np.array([3000, np.pi, 400])

for event_file in tqdm(list(event_source.glob('*'))):
    # Get event ID from file name.
    evtid = int(event_file.name)

    # Load input data.
    data = torch.load(
        event_file, map_location='cpu'
    )

    # Rescale all data
    data.x = torch.from_numpy(
        data.x.numpy()/np.array([3000, np.pi, 400])
    ).float()

    if particle_source:
        particles = pd.read_csv(
            particle_source / f'event{evtid:09}-particles.csv'
        )

        # Compute additional data.
        pt = np.sqrt(particles.px**2 + particles.py**2)
        pz = particles.pz
        p3 = np.sqrt(pt**2 + pz**2)
        p_theta = np.arccos(pz / p3)
        eta = -np.log(np.tan(0.5 * p_theta))
        
        # Construct particle information
        particles = pd.DataFrame(data={
            'pt': pt, 'eta': eta, 'pid': particles.particle_id
        })
        pid = pd.DataFrame(data={'pid': data.pid})
        pid = pid.merge(particles, on='pid', how='left')
        pid = pid.fillna(0)

        # Append to dataset
        data.pt = torch.from_numpy(pid.pt.values).float()
        data.eta = torch.from_numpy(pid.eta.values).float()

    torch.save(data, target / f'{evtid:04}')

