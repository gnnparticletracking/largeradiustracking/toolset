#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from argparse import ArgumentParser
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from torch_geometric.data import Data
from tqdm import tqdm

parser = ArgumentParser()

parser.add_argument(
    '--input', '-i',
    type=Path,
    required=True,
    help='Input directory containing npz file'
)

parser.add_argument(
    '--output', '-o',
    type=Path,
    required=True,
    help='Output directory for pyg2 file'
)

args = parser.parse_args()

source = args.input

target = args.output
target.mkdir(parents=True, exist_ok=True)

for source_file in tqdm(list(source.glob('*'))):
    # Init a new PyG data.
    pyg_data = Data()
    
    evtid = int(source_file.name)
    npz_data = np.load(source_file)
    
    # Construct particle information
    particles = pd.DataFrame(
        columns=['pid', 'pt', 'eta', 'vx', 'vy', 'vz', 'vr'],
        data=npz_data['particles']
    ).drop_duplicates(
        subset=['pid']
    )
    pid = pd.DataFrame(
        data={'pid': npz_data['pid']}
    )
    pid = pid.merge(particles, on='pid', how='left')
    pid = pid.fillna(0)

    # Copy data into PyG data
    pyg_data.hid = torch.from_numpy(npz_data['hid']).long()
    pyg_data.pid = torch.from_numpy(npz_data['pid']).long()
    pyg_data.true_edges = torch.from_numpy(npz_data['true_edges']).long()
    pyg_data.cell_data = torch.from_numpy(npz_data['cells']).float()
    pyg_data.pt = torch.from_numpy(pid.pt.values).float()
    pyg_data.eta = torch.from_numpy(pid.eta.values).float()

    # Already scaled!
    pyg_data.x = torch.from_numpy(data['x']).float()

    # Do we need full path?
    pyg_data.event_file = f'{evtid:04}'

    torch.save(pyg_data, target / f'{evtid:04}')

